/* always initiate the scroll at the top */
$(window).on('beforeunload', function () {
  $(window).scrollTop(0)
})

/* init WOW.js */
const wow = new WOW( // eslint-disable-line no-undef
  {
    offset: 30
  }
)
wow.init()

/* Draw SVG lines as user scrolls */
$(function () {
  initSVGLine('.path-to-consistency')
  initSVGLine('.path-to-market')
})

/* Get a reference to the <path> */
const initSVGLine = (elClass) => {
  const path = document.querySelector(elClass)

  // Get length of path
  const pathLength = path.getTotalLength()

  // Make very long dashes (the length of the path itself)
  path.style.strokeDasharray = `${pathLength} ${pathLength}`

  // Offset the dashes so the it appears hidden entirely
  path.style.strokeDashoffset = pathLength

  /**
  * Jake Archibald says so
  * https://jakearchibald.com/2013/animated-line-drawing-svg/
  */
  path.getBoundingClientRect()

  /* When the page scrolls... */
  window.addEventListener('scroll', function (e) {
    drawWhenVisible(elClass)
  })

  /* Check if the element is visible on screen */
  const drawWhenVisible = (el) => {
    const scrolled = $(window).scrollTop()
    const distance = $(el).offset().top
    const minOffset = $(window).height() / 2
    let velocity = 8
    const screenWidth = window.screen.width
    const scrollToVisible = distance - $(window).height() + minOffset < 0 ? 0 : distance - $(window).height() + minOffset
    // If the element is visible, then start drawing it
    if (distance - scrolled <= $(window).height() - minOffset) {
      /**
      * What % down is it?
      * https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript/2387222#2387222
      * Had to try three or four differnet methods here. Kind of a cross-browser nightmare.
      * Minus the distance to scroll to the visible area scrollToVisible
      * And accelerate the scrolling by 4
      */
      /* Check for tablet resolution to increase the line draw acceleration */
      if (screenWidth <= 768) {
        velocity = 14
      }

      const scrollPercentage = [(document.documentElement.scrollTop - scrollToVisible + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight)] * velocity

      /* Length to offset the dashes */
      const drawLength = pathLength * scrollPercentage

      /* Draw in reverse */
      path.style.strokeDashoffset = pathLength - drawLength

      /* When complete, remove the dash array, otherwise shape isn't quite sharp */
      /* Accounts for fuzzy math */
      if (scrollPercentage >= 0.99) {
        path.style.strokeDasharray = 'none'
      } else {
        path.style.strokeDasharray = `${pathLength} ${pathLength}`
      }
    } else {
      return false
    }
  }
}

/* Change header after having scrolled
   ========================================================================== */
/* Initial variables */
const maxScrolling = '100'
let scrolled, scrolledMax

$(window).bind('scroll', function () {
  getScrolledData()
})

const getScrolledData = () => {
  scrolled = $(window).scrollTop()

  /**
  * Detect if the user has scrolled more than the first section (height)
  * to then apply the changes in the header
  */
  if (scrolled >= maxScrolling) {
    headerChanges()
    scrolledMax = true
  }

  if (scrolledMax && scrolled < maxScrolling) {
    headerReset()
    scrolledMax = false
  }
}

const headerChanges = () => {
  $('.js-main-menu').addClass('solid-bg')
}

const headerReset = () => {
  $('.js-main-menu').removeClass('solid-bg')
}

$(function () {
  /**
  * This check comes from the variable $sd-max from assets/stylesheets/base/variables/media-query.scss
  * If at some point we change the value of $sd-max don't forget to chage this part as well
  */
  window.onresize = () => {
    if (window.innerWidth > 769) {
      $('.js-mobile-submenu-landing').css('display', 'block')
    } else {
      $('.js-mobile-submenu-landing').slideUp()
    }
  }
  const mainMenu = $('.js-main-menu')
  /* Toggle nav on click */
  $('.js-landing-mobile-burger').click(() => {
    if (!mainMenu.hasClass('solid-bg')) {
      mainMenu.toggleClass('solid-bg')
    }
    mainMenu.toggleClass('js-check-top')

    $('.js-mobile-submenu-landing').slideToggle(100)
    $('.js-mobile-submenu-landing').css('display', 'flex')

    /* Check if .main-menu have the class js-check-top and if scroll position is less than 100, remove class of .solid-bg */
    if (!mainMenu.hasClass('js-check-top') && $(window).scrollTop() < 100) {
      mainMenu.removeClass('solid-bg')
    }
  })
})

/* Smoothscrolling with anchors
   ========================================================================== */
$(document).on('click', '.smoothScroll', function () {
  const target = $(this).data('linkto')

  if (target.length) {
    $('html,body').animate({
      scrollTop: $(`#${target}`).offset().top
    }, 1000)
  }

  return true
})

$(function () {
  /* Create Cookie controler for acceptance and setup modification. */
  cookieController()
})

const cookieController = () => {
  /* eslint-disable no-undef */
  const cookieController = {
    // When the user accept the cookies, we write the cookies witht he configuration.
    accept: () => {
      // Cookie we use for GTM, in order to track the user needs to accept this cookie.
      Cookies.set('acceptance', 'true', { expire: 60 })
      // Cookie we use to hide the Cookies Alert once the user accepts the cookie.
      Cookies.set('cookies-preference', 'true')
      // Cookie we use to hide the Cookies Alert once the user accepts the cookie.
      Cookies.set('acceptance-remainder', 'true')
      $('.js-cookies-alert').fadeOut()
    },
    trackOff: () => {
      Cookies.remove('cookies-preference')
      Cookies.remove('acceptance')
      setTimeout(() => {
        window.location.reload(true)
      }, 1000)
    }
  }

  /* Cookie we use for GTM, in order to track the user needs to accept this cookie. */
  const acceptanceCookie = Cookies.get('acceptance')
  /* Cookie we use to remeber users prefferance (track on/off) */
  const preferenceCookies = Cookies.get('cookies-preference')
  /* Cookie we use to hide the Cookies Alert once the user accepts the cookie. */
  const acceptanceReminder = Cookies.get('acceptance-remainder')
  /* If the user never clicks Accept, we show the banner (display: none by default) */
  if (!acceptanceReminder) {
    $('.js-cookies-alert').css('display', 'flex')
  }
  /* If acceptance Cookies is present, set the cookies-preference On. */
  if (acceptanceCookie) {
    Cookies.set('cookies-preference', 'true')
  }
  /**
  * The Default setup for cookies is off (acceptance = undefined )
  * If user accept the cookies, write te cookie and fire the GTM events.
  */
  $('.js-cookies-accept').on('click', () => {
    cookieController.accept()
  })
  /**
  * If cookies are accepted, we turn ON the cookie toggle configuration in our cookies page.
  * If the box is clicked again, we turn tracking and toggle off.
  */
  if (preferenceCookies) {
    $('.js-analytics-tracking').prop('checked', true)
    $('.js-analytics-tracking').on('click', () => {
      if (preferenceCookies) {
        cookieController.trackOff()
      }
    })
  }
  /* When status of the toggle changes from off to on, restore the cookies. */
  if (!preferenceCookies) {
    $('.js-analytics-tracking').on('click', () => {
      cookieController.accept()
    })
  }
}

/* eslint-disable */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M699XPJ')
